const express = require('express')

const app = express()

const port = 3000

app.use(express.json())

app.use(express.urlencoded({extended: true}))

app.listen(port, () => console.log(`Server is running at localhost:${port}`))


// 1. Create a GET route that will access the /home route that will print out a simple message.
app.get("/home", (request, response) => {
	response.send('Welcome to the home page')
})

// 2. Process a GET request at the /home route using postman.
/* DONE*/

// 3. Create a GET route that will access the /users route that will retrieve all the users in the mock database.
let users = [
	{
		"username": "johndoe",
		"password": "johndoe1234" 
	}
]

app.get("/users", (request, response) => {
	response.send(users)
})

// 4. Process GET request at the /users route using postman.
/*DONE*/

// 5. Create DELETE route that will access the /delete-user route to remove a user from the mock database.
app.delete("/delete-user", (request, response) => {

	let message = "User does not exist!";

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users.splice(i, 1);
			message = `User ${request.body.username} has been deleted`

			break;
			}
	}

	response.send(message);
})